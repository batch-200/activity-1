/**
 * Q1. How do you create array in JS
 * A1. Using an array literal is the easiest way to create a JavaScript Array.
 *
 * Syntax:
 * const array_name = [item1, item2, ...]
 */

/**
 * Q2. How do you access first character of an array
 * A2. You access an array element by referring to the index number:
 * A2. Array index starts with 0
 *
 * Syntax:
 * array[0]
 */

/**
 * Q3. What array method searches for, and returns the index of a given value in an array?
 * A3. Array.indexOf(searchElement, fromIndex)
 * A3. The fromIndex argument can be a positive or negative integer.
 *      If the fromIndex argument is negative, the indexOf() method starts searching at array’s length plus fromIndex
 *
 * Ex:
 * const scores = [10, 20, 30, 10, 40, 20];
 *
 * console.log(scores.indexOf(10)); // 0
 * console.log(scores.indexOf(30)); // 2
 * console.log(scores.indexOf(50)); // -1
 * console.log(scores.indexOf(20)); // 1
 *
 * Note: the indexes doest change
 * console.log(scores.indexOf(20,-1)); // 5 (fromIndex = 6 + (-1) = 5)
 * console.log(scores.indexOf(20,-5)); // 1 (fromIndex = 6 + (-5) = 1)
 *
 * Function to find the indices of all occurrences of an element in an array.
 *
 * function find(needle, haystack) {
 *   var results = [];
 *   var idx = haystack.indexOf(needle);
 *   while (idx != -1) {
 *       results.push(idx);
 *       idx = haystack.indexOf(needle, idx + 1);
 *   }
 *   return results;
 * }
 *
 * console.log(find(10,scores)); // [0, 3]
 *
 * The lastIndexOf() method returns the index of the last occurrence of the searchElement in the array.
 * It returns -1 if it cannot find the element.
 *
 * Syntax:
 * console.log(scores.lastIndexOf(10));// 3
 * console.log(scores.lastIndexOf(20));// 5
 *
 */

/**
 * Q4. What array method loops over all elements of an array, performing a user-defined function on each iteration?
 *
 * A4. Typically, when you want to execute a function on every element of an array, you use a for loop statement
 *
 * Syntax:
 * let ranks = ['A', 'B', 'C'];
 * for (let i = 0; i < ranks.length; i++) {
 *   console.log(ranks[i]);
 * }
 *
 * Output:
 * A
 * B
 * C
 *
 * JavaScript Array provides the forEach() method that allows you to run a function on every element also.
 *
 * Syntax:
 * ranks.forEach(function (e) {
 *   console.log(e);
 * });
 *
 * Output:
 * Same with for loop above
 */

/**
 * Q5. What array method creates a new array with elements obtained from a user-defined function?
 * A5. The map() method creates a new array with the results of a function call on each element in the array.
 *
 * let fish = [ "piranha", "barracuda", "cod", "eel" ];

 * Print out each item in the array
 * let printFish = fish.map(individualFish => {
 *	console.log(individualFish);
 * });
 *
 * printFish;
 *
 * Output:
 * piranha
 * barracuda
 * cod
 * eel
 */

/**
  * Q6. What array method checks if all its elements satisfy a given condition?
  * A6. The filter() method creates a new array with the elements that pass the result of a given test.
  * 
  * let seaCreatures = [ "shark", "whale", "squid", "starfish", "narwhal" ];

  * Filter all creatures that start with "s" into a new list
  *
  * let filteredList = seaCreatures.filter(creature => {
  * return creature[0] === "s";
  * });
  *
  * filteredList;
  * Output
  * [ 'shark', 'squid', 'starfish' ]
  */

/**
 * Q7. What array method checks if at least one of its elements satisfies a given condition?
 * A7. The some() method checks whether at least one element of the calling array satisfy the condition.
 *
 * Syntax:
 * Note that some() stops the iteration of the array and exits as soon as the condition is satisfied.
 *
 * var arr = [1, 2, 3, 4, 5];
 *
 * arr.some(function(element, index) {
 *	console.log(element);
 *
 *	if(element > 3)
 *		return true;
 *	else
 *		return false;
 * });
 *
 * Output:
 * True
 */

/**
 * Q8. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
 * A8. False
 * Note that the splice() method actually changes the original array.
 * Also, the splice() method does not remove any elements, therefore, it returns an empty array.
 */

/**
 * Q9. True or False: array.slice() copies elements from original array and returns them as a new array.
 * A9. True
 */

/**
 * Q10. Create a function named addToEnd that will add a passed in string to the end of a passed in array.
 *      If element to be added is not a string, return the string "error - can only add strings to an array".
 *      Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
 *
 */

let students = ['piranha', 'barracuda', 'cod', 'eel']

function addToEnd(strToAdd, arr) {
  if (typeof strToAdd === 'string') {
    arr.push(strToAdd)
    return fish
  } else {
    return 'error - can only add strings to an array'
  }
}

console.log(addToEnd('Ryan', students))

/**
 * Q11. Create a function named addToStart that will add a passed in string to the start of a passed in array.
 *      If element to be added is not a string, return the string "error - can only add strings to an array".
 *      Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
 */

function addToStart(strToAdd, arr) {
  if (typeof strToAdd === 'string') {
    arr.unshift(strToAdd)
    return fish
  } else {
    return 'error - can only add strings to an array'
  }
}

console.log(addToStart('Ryan', students))

/**
 * Q12. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument.
 *      If array is empty, return the message "error - passed in array is empty".
 *      Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.
 */

function elementChecker(str, arr) {
  if (arr.length & Array.isArray(arr)) {
    if (typeof str === 'string') {
      return arr.some((e) => e === str)
    } else {
      return 'error - passed in array is empty'
    }
  } else {
    return 'error - passed in array is empty'
  }
}

console.log(elementChecker('Jane', i))

/*
Q13. Create a function named checkAllStringsEnding that will accept a passed in array and a character. 
     The function will do the ff:


if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"

if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.
*/
function checkAllStringsEnding(arr, str) {
  if (Array.isArray(arr)) {
    if (arr.length) {
      if (arr.some((e) => typeof e === 'string')) {
        if (typeof str === 'string') {
          if (str.length === 1) {
            let flag = true

            for (let i = 0; i < arr.length; i++) {
              if (arr[i].slice(-1) !== str) {
                flag = false
                break
              }
            }
            return flag
          } else {
            return 'error - 2nd argument must be a single character'
          }
        } else {
          return 'error - 2nd argument must be of data type string'
        }
      } else {
        return 'error - all array elements must be strings'
      }
    } else {
      return 'error - array must NOT be empty'
    }
  } else {
    return 'error - must be an array'
  }
}

/**
 * Create a function named stringLengthSorter that will take in an array of strings as its argument
 * and sort its elements in an ascending order based on their lengths. If at least one element is not a string,
 * return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
 *
 */

function stringLengthSorter(arr) {
  //sort its elements in an ascending order based on their lengths
  arr.sort((a, b) => a.length - b.length)
  for (let i = 0; i < arr.length; i++) {
    if (typeof arr[i] === 'string') {
    } else {
      return 'error - all array elements must be strings'
    }
  }
  return arr
}

/*
Q14. Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:

if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
return the number of elements in the array that start with the character argument, must be case-insensitive

Use the students array and the character "J" as arguments when testing.
*/

function startsWithCounter(arr, char) {
  if (arr.length < 1) {
    return 'error - array must NOT be empty'
  }

  if (arr.some((a) => typeof a !== 'string')) {
    return 'error - all array elements must be strings'
  }

  if (typeof char !== 'string') {
    return 'error - 2nd argument must be of data type string'
  }

  if (char.length > 1) {
    return 'error - 2nd argument must be a single character'
  }

  const count = 0

  arr.forEach((element) => {
    if (element[0].toLowerCase() === char.toLowerCase()) {
      count++
    }
  })

  return count
}

//Q15. Random Picker
//Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked.
//Pass in the students array as an argument when testing.

const randomPicker = (array) => {
  console.log(array[Math.floor(Math.random() * array.length)])
  return array[Math.floor(Math.random() * array.length)]
}

randomPicker(students)
randomPicker(students)
randomPicker(students)
